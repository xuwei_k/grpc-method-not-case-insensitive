import bar.foo.{BazGrpc, XXX}
import io.grpc.ManagedChannelBuilder

object Client {
  def main(args: Array[String]): Unit = {
    val channel = ManagedChannelBuilder.forAddress("localhost", 4567).usePlaintext(true).build()
    val client = BazGrpc.blockingStub(channel)
    println(client.aaa(XXX()))
  }
}