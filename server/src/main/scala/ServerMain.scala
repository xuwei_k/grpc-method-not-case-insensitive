import bar.foo.{BazGrpc, ServerImpl}
import io.grpc.netty.NettyServerBuilder
import scala.concurrent.ExecutionContext
import scala.util.Try

object ServerMain {
  def main(args: Array[String]): Unit = {
    val port = Try(args.head.toInt).getOrElse(4567)
    val server = NettyServerBuilder.forPort(port).addService(
      BazGrpc.bindService(ServerImpl, ExecutionContext.global)
    ).build

    server.start()
    server.awaitTermination()
  }
}
