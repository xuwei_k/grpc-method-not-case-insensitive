import com.trueaccord.scalapb.{ScalaPbPlugin => PB}

val common = PB.protobufSettings ++ Seq(
  fork in run := true,
  scalaVersion := "2.11.8",
  libraryDependencies += "com.trueaccord.scalapb" %% "scalapb-runtime-grpc" % (PB.scalapbVersion in PB.protobufConfig).value,
//  libraryDependencies += "io.netty" % "netty-tcnative-boringssl-static" % "1.1.33.Fork23",
  libraryDependencies += "io.grpc" % "grpc-all" % "1.0.1",
  PB.runProtoc in PB.protobufConfig := { args =>
    com.github.os72.protocjar.Protoc.runProtoc("-v300" +: args.toArray)
  }
)

val server = project.settings(common)
val client = project.settings(common)
